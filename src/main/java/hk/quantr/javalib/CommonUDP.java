package hk.quantr.javalib;

import java.net.DatagramPacket;
import java.net.InetAddress;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class CommonUDP {
	
	public static DatagramPacket getDatagramPacket(long x, InetAddress address, int port, int noOfByte) {
		byte bytes[] = CommonLib.getByteArrayReverse((short) x);
		for (byte b : bytes) {
			System.out.printf("< %02x\n", b);
		}
		return new DatagramPacket(bytes, bytes.length, address, port);
	}
	
	public static byte[] ipToByteArray(String ip) {
		String strs[] = ip.split("\\.");
		return new byte[]{(byte) Integer.parseInt(strs[0]), (byte) Integer.parseInt(strs[1]), (byte) Integer.parseInt(strs[2]), (byte) Integer.parseInt(strs[3])};
	}
	
}
