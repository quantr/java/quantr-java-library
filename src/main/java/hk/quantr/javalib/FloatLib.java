package hk.quantr.javalib;

import java.math.BigDecimal;

/**
 *
 * @author Lucas
 */
public class FloatLib {

	public static byte[] toBytes32Bits(float f) {
		return toBytes32Bits(String.valueOf(f));
	}

	public static byte[] toBytes32Bits(String f) {

		String final_version = "";
		byte b_array[] = new byte[4];
		byte bits = 0;
		String temp = "";
		//sign
		String sign = "";
		if (f.charAt(0) == '-') {
			sign += "1";
			f = f.replace("-", "");
		} else {
			sign += "0";
		}
		final_version += sign;

		//split into 2 parts
		BigDecimal number = new BigDecimal(f);

		//special case : zero
		if (number.compareTo(BigDecimal.ZERO) == 0) {
			final_version += "-00000000-00000000000000000000000";
			final_version = final_version.replace("-", "");
			//System.out.println(final_version);
			for (int i = 3; i >= 0; i--) {
				temp = final_version.substring(0, 8);
				final_version = final_version.substring(8);
				b_array[i] = (byte) Integer.parseInt(temp, 2);
			}
			return b_array;
		}

		//distinguish integer and decimal numbers
		String[] a = new String[2];
		if (f.contains(".") == false) {
			a[0] = f;
			a[1] = "0";
		} else {
			a = f.split("\\.");
			a[1] = "0." + a[1];
		}

		//1st part + exponent(f>=1)
		BigDecimal one = new BigDecimal(1);
		int exponent = 0;
		String firstbinary = "";
		long b = Long.parseLong(a[0]);
		firstbinary = Long.toBinaryString(b);
		if (number.compareTo(one) == 0 || number.compareTo(one) == 1) {
			exponent = firstbinary.length() - 1;
			exponent += 127;
		}
		//2nd part
		String secondbinary = "";
		//special_secondbinary is for 0.xxxxxx
		String special_secondbinary = "";
		String p = a[1];
		Double y = Double.parseDouble(p);
		if (number.compareTo(one) == 0 || number.compareTo(one) == 1) {
			for (int i = 0; i < 24; i++) {
				y *= 2;
				if (y >= 1) {
					secondbinary += "1";
					y -= 1;
					if (y == 0.0) {
						break;
					}
				} else {
					secondbinary += "0";
				}
			}
		} else if (number.compareTo(one) == -1 && number.compareTo(BigDecimal.ZERO) == 1) {
			do {
				y *= 2;
				if (y >= 1) {
					y -= 1;
					special_secondbinary += "1";
					break;
				}
				special_secondbinary += "0";
			} while (y < 1);

			for (int i = 0; i < 23; i++) {
				if (y == 0.0) {
					break;
				}
				y *= 2;
				if (y >= 1) {
					secondbinary += "1";
					y -= 1;
					if (y == 0.0) {
						break;
					}
				} else {
					secondbinary += "0";
				}
			}
		}

		//mantissa	(f>=1)
		String mantissa = "";
		if (number.compareTo(one) == 0 || number.compareTo(one) == 1) {
			mantissa = firstbinary.substring(1) + secondbinary;
			if (mantissa.length() > 23) {
				mantissa = mantissa.substring(0, 23);
			} else if (mantissa.length() < 23) {
				int k = 23 - mantissa.length();
				for (int i = 0; i < k; i++) {
					mantissa += "0";
				}
			}
		}

		//1st part + exponent(1>f & f>0)
		int count = 0;
		if (number.compareTo(one) == -1 && number.compareTo(BigDecimal.ZERO) == 1) {
			String unnormalized = firstbinary + "." + special_secondbinary;
			//System.out.println("special_secondbinary " + special_secondbinary);
			//System.out.println("unnormalized " + unnormalized);
			BigDecimal normalized = new BigDecimal(unnormalized);

			BigDecimal ten = new BigDecimal("10");
			while ((normalized.compareTo(one) == -1)) {
				normalized = normalized.multiply(ten);
				exponent -= 1;
				count++;

			}
			//System.out.println("normalized " + normalized);
			//System.out.println("count" + count);
			exponent += 127;
		}

		//mantissa (1>f & f>0)
		if (number.compareTo(one) == -1 && number.compareTo(BigDecimal.ZERO) == 1) {
			mantissa = secondbinary;

			if (mantissa.length() > 23) {
				mantissa = mantissa.substring(0, 23);
			} else if (mantissa.length() < 23) {
				int k = 23 - mantissa.length();
				for (int i = 0; i < k; i++) {
					mantissa += "0";
				}
			}

		}
		//ensure exponent 8-bit
		String adjusted_ex = Integer.toBinaryString(exponent);
		if (adjusted_ex.length() < 8) {
			int m = 8 - adjusted_ex.length();
			for (int i = 0; i < m; i++) {
				adjusted_ex = "0" + adjusted_ex;
			}
		}

		final_version += "-" + adjusted_ex + "-" + mantissa;

		//System.out.println("input= " + f);
		//System.out.println("a[0] " + a[0]);
		//System.out.println("a[1] " + a[1]);
		//System.out.println("exponent= " + exponent);
		//System.out.println("firstbinary= " + firstbinary);
		//System.out.println("secondbinary= " + secondbinary);
		//System.out.println("mantissa= " + mantissa);
		//System.out.println("final_version= " + final_version);
		//return byte array
		final_version = final_version.replace("-", "");
		//System.out.println("final_version= " + final_version);

		for (int i = 3; i >= 0; i--) {
			temp = final_version.substring(0, 8);
			final_version = final_version.substring(8);
			b_array[i] = (byte) Integer.parseInt(temp, 2);

		}
		return b_array;
	}
}
