package hk.quantr.javalib;

import java.io.IOException;
import java.util.Properties;

public class PropertyUtil {

	static Properties prop = null;

	/**
	 * Get the propertiy from quantrjavalib.properties
	 *
	 * @param name name of the property
	 * @return the value of the property
	 *
	 */
	public static String getProperty(String name) {
		return getProperty("quantrjavalib.properties", name);
	}

	/**
	 * Get the propertiy from a specific properties file
	 *
	 * @param propertyFile file name/path in resources
	 * @param name of the property
	 * @author Peter Cheung
	 *
	 * @return the value of the property
	 *
	 */
	public static String getProperty(String propertyFile, String name) {
		if (prop == null) {
			prop = new Properties();
			try {
				prop.load(PropertyUtil.class.getClassLoader().getResourceAsStream(propertyFile));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return prop.getProperty(name);
	}

	public static Properties getLangProperty(String lang) {
		Properties langProp = new Properties();
		try {
			langProp.load(PropertyUtil.class.getClassLoader().getResourceAsStream("lang_" + lang + ".properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return langProp;
	}
}
