package hk.quantr.javalib.swing;

import javax.swing.tree.DefaultMutableTreeNode;

public abstract class FilterTreeNode extends DefaultMutableTreeNode {
	public boolean isShown = true;
}
