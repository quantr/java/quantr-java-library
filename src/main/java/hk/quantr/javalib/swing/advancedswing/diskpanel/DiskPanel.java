package hk.quantr.javalib.swing.advancedswing.diskpanel;

import hk.quantr.javalib.CommonLib;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;

import org.apache.commons.io.IOUtils;

import hk.quantr.javalib.mbr.ReadMBR;
import hk.quantr.javalib.swing.advancedswing.pager.Pager;
import hk.quantr.javalib.swing.advancedswing.pager.PagerEvent;
import hk.quantr.javalib.swing.advancedswing.pager.PagerEventListener;
import hk.quantr.javalib.swing.advancedswing.pager.PagerTextFieldEvent;
import hk.quantr.javalib.swing.advancedswing.pager.PagerTextFieldEventListener;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

@SuppressWarnings("serial")
public class DiskPanel extends JPanel {

	public JScrollPane scrollPane1;
	public JComboBox<String> columnCountComboBox;
	public JLabel colCountLabel;
	public JComboBox<String> rowCountComboBox;
	public JLabel rowCountLabel;
	public JComboBox<String> radixComboBox;
	public Pager pager;
	public JToolBar toolbar;
	public JTable table;
	public DiskTableModel model = new DiskTableModel();
	public File file;
	private JButton showMBRButton;

	public DiskPanel() {
		initGUI();
	}

	public DiskPanel(File file) {
		this();
		this.file = file;
		model.setFile(file);
	}

	public File getFile() {
		return file;
	}

	private void initGUI() {
		try {
			BorderLayout thisLayout = new BorderLayout();
			this.setLayout(thisLayout);

			scrollPane1 = new JScrollPane();
			this.add(scrollPane1, BorderLayout.CENTER);

			table = new JTable();
			table.setRowSelectionAllowed(false);
			table.getTableHeader().setReorderingAllowed(false);
			table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			scrollPane1.setViewportView(table);
			table.setModel(model);

			for (int x = 1; x <= 9; x++) {
				table.getColumnModel().getColumn(x).setPreferredWidth(40);
			}

			DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
			centerRenderer.setHorizontalAlignment(SwingConstants.CENTER);
			for (int x = 1; x < table.getColumnCount() - 1; x++) {
				table.getColumnModel().getColumn(x).setPreferredWidth(40);
				table.getColumnModel().getColumn(x).setCellRenderer(centerRenderer);
			}

			toolbar = new JToolBar();
			this.add(toolbar, BorderLayout.NORTH);

			pager = new Pager();
			pager.setPageNo(0);
			pager.setMaximumSize(new Dimension(180, 23));
			toolbar.add(pager);
			pager.addPagerTextFieldEventListener(new PagerTextFieldEventListener() {
				public void KeyReleased(PagerTextFieldEvent evt) {
					pager1KeyReleased(evt);
				}
			});
			pager.addPagerEventListener(new PagerEventListener() {
				public void clicked(PagerEvent evt) {
					pager1Clicked(evt);
				}
			});

			ComboBoxModel<String> jRadixComboBoxModel = new DefaultComboBoxModel<String>(new String[] { "2", "8", "10", "16" });
			radixComboBox = new JComboBox<String>();
			radixComboBox.setOpaque(false);
			toolbar.add(radixComboBox);
			radixComboBox.setModel(jRadixComboBoxModel);
			radixComboBox.setSelectedItem("16");
			radixComboBox.setMaximumSize(new Dimension(100, 23));
			radixComboBox.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent evt) {
					radixComboBoxActionPerformed(evt);
				}
			});

			rowCountLabel = new JLabel();
			toolbar.add(rowCountLabel);
			rowCountLabel.setText("Row count");

			ComboBoxModel<String> jRowCountComboBoxModel = new DefaultComboBoxModel<String>(new String[] { "10", "20", "50", "100" });
			rowCountComboBox = new JComboBox<String>();
			rowCountComboBox.setOpaque(false);
			toolbar.add(rowCountComboBox);
			rowCountComboBox.setModel(jRowCountComboBoxModel);
			rowCountComboBox.setSelectedItem("100");
			rowCountComboBox.setEditable(true);
			rowCountComboBox.setMaximumSize(new Dimension(100, 23));
			rowCountComboBox.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent evt) {
					rowCountComboBoxActionPerformed(evt);
				}
			});

			colCountLabel = new JLabel();
			toolbar.add(colCountLabel);
			colCountLabel.setText("Col count");

			ComboBoxModel<String> jComboBox1Model = new DefaultComboBoxModel<String>(new String[] { "8", "10", "16", "20", "24", "30" });
			columnCountComboBox = new JComboBox<String>();
			columnCountComboBox.setOpaque(false);
			toolbar.add(columnCountComboBox);
			columnCountComboBox.setModel(jComboBox1Model);
			columnCountComboBox.setEditable(true);
			columnCountComboBox.setMaximumSize(new Dimension(100, 23));

			showMBRButton = new JButton("Show MBR");
			showMBRButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					JTextArea textArea = new JTextArea(30, 70);
					String s = "";
					try {
						InputStream in = new FileInputStream("/Users/peter/workspace/GKD/vm/dlxlinux/hd10meg.img");

						in.skip(0x1be);
						for (int x = 0; x < 4; x++) {
							byte partition[] = new byte[16];
							IOUtils.read(in, partition, 0, 16);
							s += ReadMBR.getPartitionTable(partition) + "\n";
						}
						IOUtils.closeQuietly(in);
					} catch (Exception ex) {
						ex.printStackTrace();
					}

					textArea.setText(s);
					textArea.setEditable(false);
					JScrollPane scrollPane = new JScrollPane(textArea);
					JOptionPane.showMessageDialog(null, scrollPane);
				}
			});
			toolbar.add(showMBRButton);
			columnCountComboBox.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent evt) {
					columnCountComboBoxActionPerformed(evt);
				}
			});
			model.setRadix(16);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setFile(File file) {
		this.file = file;
		model.setFile(file);
	}

	private void pager1Clicked(PagerEvent evt) {
		int pageSize = (model.getByteColumnCount()) * model.getRowCount();
		if (evt.getType() == PagerEvent.FIRST_PAGE_BUTTON) {
			model.setOffset(0);
		} else if (evt.getType() == PagerEvent.PREVIOUS_PAGE_BUTTON) {
			model.setOffset(model.getOffset() - pageSize);
		} else if (evt.getType() == PagerEvent.NEXT_PAGE_BUTTON) {
			model.setOffset(model.getOffset() + pageSize);
		} else if (evt.getType() == PagerEvent.LAST_PAGE_BUTTON) {
			model.setOffset(file.length() - pageSize);
		}
		pager.setPageNo((int) (model.getOffset()));
	}

	private void radixComboBoxActionPerformed(ActionEvent evt) {
		model.setRadix(Integer.parseInt(radixComboBox.getSelectedItem().toString()));
	}

	private void pager1KeyReleased(PagerTextFieldEvent evt) {
		long offset = 0;
		try {
			offset = CommonLib.string2long(evt.getValue());
		} catch (Exception ex) {
			offset = CommonLib.convertFilesize(evt.getValue());
		}
		model.setOffset(offset);
	}

	private void rowCountComboBoxActionPerformed(ActionEvent evt) {
		model.setRowCount(Integer.parseInt(rowCountComboBox.getSelectedItem().toString()));
		model.fireTableStructureChanged();
		model.fireTableDataChanged();
	}

	private void columnCountComboBoxActionPerformed(ActionEvent evt) {
		model.setColumnCount(Integer.parseInt(columnCountComboBox.getSelectedItem().toString()));
		model.fireTableStructureChanged();
		model.fireTableDataChanged();

		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment(SwingConstants.CENTER);
		for (int x = 1; x < table.getColumnCount() - 1; x++) {
			table.getColumnModel().getColumn(x).setPreferredWidth(40);
			table.getColumnModel().getColumn(x).setCellRenderer(centerRenderer);
		}
	}

}
