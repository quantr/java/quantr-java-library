package hk.quantr.javalib.swing.advancedswing.highdpijlabel;

import hk.quantr.javalib.CommonLib;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JViewport;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class HighDPIJLabel extends JLabel implements MouseListener, MouseMotionListener {

	public int padding;
	public float ratio = 1.0f;
	int lastX;
	int lastY;
	JScrollPane jScrollPane;

	@Override
	public void paint(Graphics g) {
		int imageDrawWidth = 0;
		if (this.getIcon() != null) {
			BufferedImage image = CommonLib.toBufferedImage(CommonLib.iconToImage(this.getIcon()));

			Graphics2D g2d = (Graphics2D) g;
			g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

			int imageDrawHeight = getHeight() - padding;
			imageDrawWidth = image.getWidth(null) * imageDrawHeight / image.getHeight(null);
			g2d.drawImage(image, (getWidth() - imageDrawWidth) / 2, padding / 2, (int) (imageDrawWidth * ratio), (int) (imageDrawHeight * ratio), null);
		}
		if (this.getText() != null && !this.getText().equals("")) {
			g.setColor(Color.black);
			int fontHeight = (int) getFont().createGlyphVector(g.getFontMetrics().getFontRenderContext(), getText()).getVisualBounds().getHeight();
			g.drawString(getText(), imageDrawWidth + 4, getHeight() - ((getHeight() - fontHeight) / 2));
		}
	}

	public void addDrag(JScrollPane jScrollPane) {
		this.jScrollPane = jScrollPane;
		this.addMouseListener(this);
		this.addMouseMotionListener(this);
	}

	@Override
	public void mousePressed(MouseEvent e) {
		if (System.getProperty("os.name").startsWith("Mac OS X")) {
			setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		} else {
			setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
		}
		lastX = e.getX();
		lastY = e.getY();
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
	}

	@Override
	public void mouseDragged(MouseEvent evt) {
		Container c = getParent();
		if (c instanceof JViewport) {
			JViewport jv = (JViewport) c;
			Point p = jv.getViewPosition();
			int newX = p.x - (evt.getX() - lastX);
			int newY = p.y - (evt.getY() - lastY);

			int maxX = getWidth() - jv.getWidth();
			int maxY = getHeight() - jv.getHeight();
			if (newX < 0) {
				newX = 0;
			}
			if (newX > maxX) {
				newX = maxX;
			}
			if (newY < 0) {
				newY = 0;
			}
			if (newY > maxY) {
				newY = maxY;
			}

			jv.setViewPosition(new Point(newX, newY));
		}
	}

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	@Override
	public void mouseMoved(MouseEvent e) {
	}

}
