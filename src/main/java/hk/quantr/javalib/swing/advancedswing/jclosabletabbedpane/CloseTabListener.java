package hk.quantr.javalib.swing.advancedswing.jclosabletabbedpane;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public interface CloseTabListener {

	public boolean closeTab(int tabIndex, String tabTitle, Object component);
}
