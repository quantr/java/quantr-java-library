package hk.quantr.javalib.swing.advancedswing.jprogressbardialog;

import java.util.EventListener;

public interface JProgressBarDialogEventListener extends EventListener {
	public void cancelled();
}
