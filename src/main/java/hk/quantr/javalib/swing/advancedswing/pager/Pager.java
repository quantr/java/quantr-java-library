package hk.quantr.javalib.swing.advancedswing.pager;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.EventListenerList;

public class Pager extends JPanel {
	private JButton firstPageButton;
	private JButton nextPageButton;
	private JButton lastPageButton;
	private JTextField pageNoTextField;
	private JButton previousPageButton;
	protected EventListenerList listenerList = new EventListenerList();
	protected EventListenerList textFieldlistenerList = new EventListenerList();
	public int maxPageNo = 1;

	public Pager() {
		super();
		try {
			FlowLayout thisLayout = new FlowLayout(0, 0, 0);
			thisLayout.setAlignment(FlowLayout.LEFT);
			this.setLayout(thisLayout);
			this.setOpaque(false);

			firstPageButton = new JButton();
			this.add(firstPageButton);
			firstPageButton.setText("|<");
			firstPageButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent evt) {
					firstPageButtonActionPerformed(evt);
				}
			});

			previousPageButton = new JButton();
			this.add(previousPageButton);
			previousPageButton.setText("<");
			previousPageButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent evt) {
					previousPageButtonActionPerformed(evt);
				}
			});

			pageNoTextField = new JTextField("1");
			pageNoTextField.setHorizontalAlignment(JTextField.CENTER);
			this.add(pageNoTextField);
			pageNoTextField.setPreferredSize(new java.awt.Dimension(65, 22));
			pageNoTextField.addKeyListener(new KeyAdapter() {
				public void keyReleased(KeyEvent evt) {
					pageNoTextFieldKeyReleased(evt);
				}
			});

			nextPageButton = new JButton();
			this.add(nextPageButton);
			nextPageButton.setText(">");
			nextPageButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent evt) {
					nextPageButtonActionPerformed(evt);
				}
			});

			lastPageButton = new JButton();
			this.add(lastPageButton);
			lastPageButton.setText(">|");
			lastPageButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent evt) {
					lastPageButtonActionPerformed(evt);
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void addPagerEventListener(PagerEventListener listener) {
		listenerList.add(PagerEventListener.class, listener);
	}

	public void removePagerEventListener(PagerEventListener listener) {
		listenerList.remove(PagerEventListener.class, listener);
	}

	public void addPagerTextFieldEventListener(PagerTextFieldEventListener listener) {
		textFieldlistenerList.add(PagerTextFieldEventListener.class, listener);
	}

	public void removePagerTextFieldEventListener(PagerTextFieldEventListener listener) {
		textFieldlistenerList.remove(PagerTextFieldEventListener.class, listener);
	}

	void firePagerEvent(PagerEvent evt) {
		Object[] listeners = listenerList.getListenerList();

		for (int i = 0; i < listeners.length; i += 2) {
			if (listeners[i] == PagerEventListener.class) {
				((PagerEventListener) listeners[i + 1]).clicked(evt);
			}
		}
	}

	void fireTextFieldReleasedEvent(PagerTextFieldEvent evt) {
		Object[] listeners = textFieldlistenerList.getListenerList();

		for (int i = 0; i < listeners.length; i += 2) {
			if (listeners[i] == PagerTextFieldEventListener.class) {
				((PagerTextFieldEventListener) listeners[i + 1]).KeyReleased(evt);
			}
		}
	}

	private void nextPageButtonActionPerformed(ActionEvent evt) {
		int pageNo = Integer.parseInt(pageNoTextField.getText());
		pageNo += 1;
		if (pageNo <= maxPageNo) {
			pageNoTextField.setText(String.valueOf(pageNo));
		}
		PagerEvent event = new PagerEvent(this);
		event.setType(2);
		firePagerEvent(event);
	}

	private void previousPageButtonActionPerformed(ActionEvent evt) {
		int pageNo = Integer.parseInt(pageNoTextField.getText());
		pageNo -= 1;
		if (pageNo >= 1) {
			pageNoTextField.setText(String.valueOf(pageNo));
		}
		PagerEvent event = new PagerEvent(this);
		event.setType(1);
		firePagerEvent(event);
	}

	private void firstPageButtonActionPerformed(ActionEvent evt) {
		pageNoTextField.setText("1");
		PagerEvent event = new PagerEvent(this);
		event.setType(0);
		firePagerEvent(event);
	}

	private void lastPageButtonActionPerformed(ActionEvent evt) {
		pageNoTextField.setText(String.valueOf(maxPageNo));
		PagerEvent event = new PagerEvent(this);
		event.setType(3);
		firePagerEvent(event);
	}

	public void setPageNo(int pageNo) {
		pageNoTextField.setText(String.valueOf(pageNo));
	}

	private void pageNoTextFieldKeyReleased(KeyEvent evt) {
		PagerTextFieldEvent event = new PagerTextFieldEvent(this);
		event.setKeyCode(evt.getKeyCode());
		event.setValue(pageNoTextField.getText());
		fireTextFieldReleasedEvent(event);
	}

	public int getPage() {
		return Integer.parseInt(pageNoTextField.getText());
	}
}
