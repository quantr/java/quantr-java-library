package hk.quantr.javalib.swing.advancedswing.pager;

public interface PagerEventListener extends java.util.EventListener {
	public void clicked(PagerEvent evt);
}
