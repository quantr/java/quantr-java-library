package hk.quantr.javalib.swing.advancedswing.pager;

public interface PagerTextFieldEventListener extends java.util.EventListener {
	public void KeyReleased(PagerTextFieldEvent evt);
}
