package hk.quantr.javalib.swing.advancedswing.ribbonbar;

import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.SwingConstants;

public class JRibbonButton extends JButton {

	public JRibbonButton() {
		initGUI();
	}

	public JRibbonButton(Icon icon) {
		super(icon);
		initGUI();
	}

	public JRibbonButton(String name) {
		super(name);
		initGUI();
	}

	public JRibbonButton(Action a) {
		super(a);
		initGUI();
	}

	public JRibbonButton(String text, Icon icon) {
		super(text, icon);
		initGUI();
	}

	private void initGUI() {
		setHorizontalAlignment(SwingConstants.CENTER);
		putClientProperty("buttonType", "ribbonButton");
	}

}
