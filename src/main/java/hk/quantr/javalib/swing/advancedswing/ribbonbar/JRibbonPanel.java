package hk.quantr.javalib.swing.advancedswing.ribbonbar;

import java.awt.Graphics;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import javax.swing.plaf.ButtonUI;

public class JRibbonPanel extends JPanel {
	private JTabbedPane tabbedPane;
	Image bg = new ImageIcon(ButtonUI.class.getResource("/hk/quantr/javalib/swing/advancedswing/jribbonbar/jRibbonPanelBG.png")).getImage();

	//	public JRibbonPanel() {
	//		setLayout(new BorderLayout(0, 0));
	//
	//		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
	//		add(tabbedPane);
	//	}
	//
	//	public void addTab(String tabName) {
	//		JPanel jPanel = new JPanel();
	//		FlowLayout flowLayout = new FlowLayout();
	//		jPanel.setLayout(flowLayout);
	//		tabbedPane.add(tabName, jPanel);
	//	}

	@Override
	public void paint(Graphics g) {
		g.drawImage(bg, 0, 0, getWidth(), getHeight(), null);
		super.paintChildren(g);
	}
}
