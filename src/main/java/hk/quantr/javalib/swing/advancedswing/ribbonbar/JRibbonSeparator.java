package hk.quantr.javalib.swing.advancedswing.ribbonbar;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

public class JRibbonSeparator extends JPanel {
	private JTabbedPane tabbedPane;
	Color colorLeft = new Color(238, 238, 238);
	Color colorRight = new Color(148, 148, 148);

	@Override
	public void paint(Graphics g) {
		g.setColor(colorLeft);
		g.drawLine(0, 0, 0, getHeight());
		g.setColor(colorRight);
		g.drawLine(1, 0, 1, getHeight());
		super.paintChildren(g);
	}
}
