
import hk.quantr.javalib.FloatLib;
import org.junit.Test;

public class TestAllFloat {

	@Test
	public void test() {
		float f = 1.23f;
		for (int x = 0; x < 10000000; x++) {
			byte[] newbytes2 = FloatLib.toBytes32Bits(f);
			int bits = Float.floatToIntBits(f);
			byte newbytes[] = new byte[4];
			newbytes[0] = (byte) (bits & 0xff);
			newbytes[1] = (byte) ((bits >> 8) & 0xff);
			newbytes[2] = (byte) ((bits >> 16) & 0xff);
			newbytes[3] = (byte) ((bits >> 24) & 0xff);
			if (newbytes[1] == newbytes2[1] && newbytes[2] == newbytes2[2] && newbytes[3] == newbytes2[3]) {
				System.out.println("ok " + f);
			} else {
				System.out.println("not ok " + f);
				return;
			}
			f += -12.2387487234f;
		}
	}
}
