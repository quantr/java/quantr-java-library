
import hk.quantr.javalib.FloatLib;
import org.junit.Test;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class TestFloat {

	@Test
	public void test1() {
		byte[] newbytes2 = FloatLib.toBytes32Bits("-123.123456");
		System.out.printf("%x %x %x %x\n", newbytes2[0], newbytes2[1], newbytes2[2], newbytes2[3]);
		System.out.println("-".repeat(100));
		byte newbytes[] = new byte[4];
		int bits = Float.floatToIntBits(-123.123456f);
//		System.out.println(bits);

		newbytes[0] = (byte) (bits & 0xff);
		newbytes[1] = (byte) ((bits >> 8) & 0xff);
		newbytes[2] = (byte) ((bits >> 16) & 0xff);
		newbytes[3] = (byte) ((bits >> 24) & 0xff);

		System.out.printf("%x %x %x %x\n", newbytes[0], newbytes[1], newbytes[2], newbytes[3]);

//		System.out.print(String.format("%8s", Integer.toBinaryString(0xFF & newbytes[0])).replaceAll(" ", "0") + " ");
//		System.out.print(String.format("%8s", Integer.toBinaryString(0xFF & newbytes[1])).replaceAll(" ", "0") + " ");
//		System.out.print(String.format("%8s", Integer.toBinaryString(0xFF & newbytes[2])).replaceAll(" ", "0") + " ");
//		System.out.print(String.format("%8s", Integer.toBinaryString(0xFF & newbytes[3])).replaceAll(" ", "0") + " ");
//		System.out.println();

		if (newbytes[1] == newbytes2[1] && newbytes[2] == newbytes2[2] && newbytes[3] == newbytes2[3]) {
			System.out.println("ok");
		} else {
			System.out.println("not ok");
		}
	}
}
